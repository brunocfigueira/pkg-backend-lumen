<?php

namespace Pkgfigueira\Backend\Database\Oracle;

use Illuminate\Support\ServiceProvider;

class DatabaseOracleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {       
        //$this->publishes([__DIR__.'/migrations'=>\database_path('migrations/oracle/')]);
    }
}
