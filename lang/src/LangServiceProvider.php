<?php

namespace Pkgfigueira\Backend\Lang;

use Illuminate\Support\ServiceProvider;

class LangServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->publishes([__DIR__.'/pt_BR'=>\resource_path('lang/pt_BR')]);
    }
}
