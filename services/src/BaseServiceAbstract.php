<?php
namespace Pkgfigueira\Backend\Services;

use Illuminate\Http\Request;

abstract class BaseServiceAbstract implements IBaseService
{        
    /**
     * list all records
     */
    abstract public function findAll();
    /**
     * find unique record
     * @param type int $id
     * @return type Collection
     */
    abstract public function findById(int $id);
    /**
     * remove unique record
     * @param type int $id
     */
    abstract public function delete(int $id);
    /**
     * save data request
     * @param type Request $request
     * @return type int $id
     */
    abstract public function store(Request $request);
    /**
     * alter data request
     * @param type Request $request
     * @param type int $id
     * @return type int $id
     */
    abstract public function update(Request $request, int $id);
    /**
     * search data request
     * @param type Request $request
     * @return type mixed
     */
    abstract public function search(Request $request);
    /**
     * check exists record
     * @param type int $id
     * @return type boolean
     */
    abstract public function exists(int $id);

    /**
     * Convert value to decimal data base
     * Ex. Request = 19,90 pt_BR
     *     Convert = 19.90 en_US
     * @param type string $value
     * @return type double
     */
    public function convertDecimalValue(string $value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        return (double) $value;
    }

    public function convertSpecialCharacters(string $value)
    {
        return htmlentities(htmlspecialchars($value));
    }
    public function escapeCharactersString(string $value)
    {
        return mysql_escape($value);
    }
    /**
     *
     */
    public function makeHashFile(string $value, string $extension = 'pdf')
    {
        if (is_null($value)) {
            return $value;
        }
        return md5(uniqid($value)).'.'.$extension;// md5+uniqid = 32 characteres + length extension
    }
}
