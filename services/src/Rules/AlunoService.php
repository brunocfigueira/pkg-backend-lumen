<?php

namespace Pkgfigueira\Backend\Services\Rules;

use Pkgfigueira\Backend\Services\BaseServiceAbstract;
use Pkgfigueira\Domain\Entities\Models\Aluno;
use Illuminate\Http\Request;


class AlunoService extends BaseServiceAbstract
{

    /**
     * validation rules business for fields of table
     * @var array
     */
    private static $rules = [
        'name' => 'required|max:250',
        'descripton' => 'required|max:1000',
    ];
    /**
     * method static access for rules
     */
    public static function rules()
    {
        return self::$rules;
    }
    /**
     * list all records
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
        return Alunos::all();
    }
    /**
     * find unique record
     * @param type int $id
     * @return type Collection
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
        return Alunos::findOrFail($id);
    }
    /**
     * remove unique record
     * @param type int $id
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
        return $this->findById($id)->delete();
    }
    /**
     * save data request
     * @param type Request $request
     * @return type int $id
     */
    public function store(Request $request)
    {
        // TODO: Implement store() method.
        return Alunos::create([
            'name' => $request->name,
            'descrition' => $request->descrition,
        ]);
    }
    /**
     * alter data request
     * @param type Request $request
     * @param type int $id
     * @return type int $id
     */
    public function update(Request $request, int $id)
    {
        return Alunos::where('id', $id)->update([
            'name' => $request->name,
            'descrition' => $request->descrition,
        ]);
    }
    /**
     * search data request
     * @param type Request $request
     * @return type mixed
     */
    public function search(Request $request)
    {
        // TODO: Implement search() method.
        return Alunos::where('id', $request->id)
            ->orWhere('name', $request->name)
            ->get();
    }
    /**
     * check exists record
     * @param type int $id
     * @return type boolean
     */
    public function exists(int $id)
    {
        // TODO: Implement exists() method.
        return Menu::where('id', $id)->exists();
    }
}
