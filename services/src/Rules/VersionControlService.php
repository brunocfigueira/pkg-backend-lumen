<?php

namespace Pkgfigueira\Backend\Services\Rules;

use Illuminate\Http\Request;
use Version;
use Carbon\Carbon;

class VersionControlService
{
    /**
     * display all data
     */
    public function displayAll()
    {
        $now = Carbon::now();
        $data = [
            'label' => Version::format('full'),
            'requestDate' => $now->format('Y-m-d H:i:s'),
            'timestamp' => $now->timestamp,
        ];
        return $data;
    }
}
