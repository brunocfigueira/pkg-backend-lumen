<?php

namespace Pkgfigueira\Backend\Services;

use Illuminate\Http\Request;
//use Backend\Traits\MessagesTraits as Messages;

interface IBaseService
{
   // use Messages;
    /**
     * list all records
     */
    public function findAll();
    /**
     * get unique record
     * @param type int $id
     * @return type Collection
     */
    public function findById(int $id);
    /**
     * remove unique record
     * @param type int $id
     */
    public function delete(int $id);
    /**
     * save data request
     * @param type Request $request
     * @return type int $id
     */
    public function store(Request $request);
    /**
     * alter data request
     * @param type Request $request
     * @param type int $id
     * @return type int $id
     */
    public function update(Request $request, int $id);
    /**
     * search data request
     * @param type Request $request
     * @return type mixed
     */
    public function search(Request $request);
    
    /**
     * check exists record
     * @param type int $id
     * @return type boolean
     */
    public function exists(int $id);    
}
